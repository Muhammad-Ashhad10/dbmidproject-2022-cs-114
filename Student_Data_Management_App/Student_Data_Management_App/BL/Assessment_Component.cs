﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class Assessment_Component
    {
        public Assessment_Component() { }
        public Assessment_Component(string name, Rubric rubric, int totalMarks, DateTime dateCreated, DateTime dateUpdated, Assessment assessment)
        {
            this.name = name;
            this.rubric = rubric;
            this.totalMarks = totalMarks;
            this.dateCreated = dateCreated;
            this.dateUpdated = dateUpdated;
            this.assessment = assessment;
        }
        public Assessment_Component(int databaseID, string name, Rubric rubric, int totalMarks, DateTime dateCreated, DateTime dateUpdated, Assessment assessment)
        {
            this.databaseId = databaseID;
            this.name = name;
            this.rubric = rubric;
            this.totalMarks = totalMarks;
            this.dateCreated = dateCreated;
            this.dateUpdated = dateUpdated;
            this.assessment = assessment;
        }

        public int databaseId { get; set; }
        public string name { get; set; }
        public Rubric rubric { get; set; }
        public int totalMarks { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateUpdated { get; set; }
        public Assessment assessment { get; set; }
    }
}

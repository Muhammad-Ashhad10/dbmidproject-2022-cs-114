﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class Assessment
    {
        public Assessment() { }
        public Assessment(string title, DateTime dateCreated, int totalMarks, int totalWeightage)
        {
            this.title = title;
            this.dateCreated = dateCreated;
            this.totalMarks = totalMarks;
            this.totalWeightage = totalWeightage;
        }
        public Assessment(int databaseID, string title, DateTime dateCreated, int totalMarks, int totalWeightage)
        {
            this.databaseId = databaseID;
            this.title = title;
            this.dateCreated = dateCreated;
            this.totalMarks = totalMarks;
            this.totalWeightage = totalWeightage;
        }

        public int databaseId { get; set; }
        public string title { get; set; }
        public DateTime dateCreated { get; set; }
        public int totalMarks { get; set; }
        public int totalWeightage { get; set; }
    }
}

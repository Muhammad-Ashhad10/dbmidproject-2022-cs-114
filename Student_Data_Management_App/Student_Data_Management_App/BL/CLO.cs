﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class CLO
    {
        public CLO () { }
        public CLO(string name, DateTime dateCreated, DateTime dateUpdated)
        {
            this.name = name;
            this.dateCreated = dateCreated;
            this.dateUpdated = dateUpdated;
        }
        public CLO (int databaseID, string name, DateTime dateCreated, DateTime dateUpdated)
        {
            this.databaseID = databaseID;
            this.name = name;
            this.dateCreated = dateCreated;
            this.dateUpdated = dateUpdated;
        }

        public int databaseID { get; set; }
        public string name {  get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateUpdated { get; set; }
    }
}

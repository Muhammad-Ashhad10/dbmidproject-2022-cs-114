﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class Evaluation
    {
        public Evaluation() { }
        public Evaluation(Student student, Assessment_Component assessmentComponent, Rubric_Level rubricMeasurement, DateTime evaluationDate)
        {
            this.student = student;
            this.assessmentComponent = assessmentComponent;
            this.rubricMeasurement = rubricMeasurement;
            this.evaluationDate = evaluationDate;
        }

        public Student student { get; set; }
        public Assessment_Component assessmentComponent { get; set; }
        public Rubric_Level rubricMeasurement { get; set; }
        public DateTime evaluationDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class Rubric
    {
        public Rubric() { }
        public Rubric (string details, CLO clo)
        {
            this.details = details;
            this.clo = clo;
        }
        public Rubric (int databaseId,  string details, CLO clo)
        {
            this.databaseID = databaseId;
            this.details = details;
            this.clo = clo;
        }

        public int databaseID { get; set; }
        public string details { get; set; }
        public CLO clo {  get; set; }
    }
}

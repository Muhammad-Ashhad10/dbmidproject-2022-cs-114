﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class Rubric_Level
    {
        public Rubric_Level() { }
        public Rubric_Level(Rubric rubric, string details, int measurementLevel)
        {
            this.rubric = rubric;
            this.details = details;
            this.measurementLevel = measurementLevel;
        }
        public Rubric_Level(int databaseID, Rubric rubric, string details, int measurementLevel)
        {
            this.databaseID = databaseID;
            this.rubric = rubric;
            this.details = details;
            this.measurementLevel = measurementLevel;
        }

        public int databaseID { get; set; }
        public Rubric rubric { get; set; }
        public string details { get; set; }
        public int measurementLevel { get; set; }
    }
}

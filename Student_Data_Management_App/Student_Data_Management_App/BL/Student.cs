﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.BL
{
    class Student
    {
        public Student() { }
        public Student(string firstName, string lastName, string contactNumber, string email, string registrationNumber, string status)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.contactNumber = contactNumber;
            this.email = email;
            this.registrationNumber = registrationNumber;
            this.status = status;
        }
        public Student(int ID, string firstName, string lastName, string contactNumber, string email, string registrationNumber, string status)
        {
            this.databaseID = ID;
            this.firstName = firstName;
            this.lastName = lastName;
            this.contactNumber = contactNumber;
            this.email = email;
            this.registrationNumber = registrationNumber;
            this.status = status;
        }
        public int databaseID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string contactNumber { get; set; }
        public string email { get; set; }
        public string registrationNumber { get; set; }
        public string status { get; set; }
    }
}

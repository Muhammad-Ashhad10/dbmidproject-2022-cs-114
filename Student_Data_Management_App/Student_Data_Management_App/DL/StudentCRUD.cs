﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Student_Data_Management_App.BL;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace Student_Data_Management_App.DL
{
    class StudentCRUD
    {
        public static Student GetStudent(string registrationNumber)
        {
            Student requiredStudent;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, RegistrationNumber, FirstName, LastName, Contact, Email, (Select Name FROM Lookup WHERE LookupId = Status) AS Status FROM Student WHERE RegistrationNumber = @RegNumber";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@RegNumber", registrationNumber);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredStudent = new Student();
                requiredStudent.databaseID = int.Parse(dr["Id"].ToString());
                requiredStudent.registrationNumber = dr["RegistrationNumber"].ToString();
                requiredStudent.firstName = dr["FirstName"].ToString();
                requiredStudent.lastName = dr["LastName"].ToString();
                requiredStudent.contactNumber = dr["Contact"].ToString();
                requiredStudent.email = dr["Email"].ToString();
                requiredStudent.status = dr["Status"].ToString();

                /*if (dr["Status"].ToString() == "5")
                {
                    requiredStudent.status = "Active";
                }
                else if (dr["Status"].ToString() == "6")
                {
                    requiredStudent.status = "InActive";
                }*/
            }
            else
            {
                requiredStudent = null;
            }

            dr.Close();

            return requiredStudent;
        }
        public static Student GetStudentByDatabaseID(int databaseID)
        {
            Student requiredStudent;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, RegistrationNumber, FirstName, LastName, Contact, Email, (Select Name FROM Lookup WHERE LookupId = Status) AS Status FROM Student WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@RegNumber", databaseID);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredStudent = new Student();
                requiredStudent.databaseID = int.Parse(dr["Id"].ToString());
                requiredStudent.registrationNumber = dr["RegistrationNumber"].ToString();
                requiredStudent.firstName = dr["FirstName"].ToString();
                requiredStudent.lastName = dr["LastName"].ToString();
                requiredStudent.contactNumber = dr["Contact"].ToString();
                requiredStudent.email = dr["Email"].ToString();
                requiredStudent.status = dr["Status"].ToString();

                /*if (dr["Status"].ToString() == "5")
                {
                    requiredStudent.status = "Active";
                }
                else if (dr["Status"].ToString() == "6")
                {
                    requiredStudent.status = "InActive";
                }*/
            }
            else
            {
                requiredStudent = null;
            }

            dr.Close();

            return requiredStudent;
        }
        public static int GetStudentDatabaseID(string registrationNumber)
        {
            int databaseID = -1;

            var con = Configuration.getInstance().getConnection();

            string selectQuery = "SELECT Id FROM Student WHERE RegistrationNumber = @regNum";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@regNum", registrationNumber);
            SqlDataReader dr = selectCommand.ExecuteReader();

            if (dr.Read())
            {
                databaseID = Convert.ToInt32(dr["Id"]);
            }

            dr.Close ();

            return databaseID;
        }
        public static List<string> GetAllActiveRegistrationNumbers()
        {
            List<string> registationNumbers = new List<string>();

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT RegistrationNumber FROM Student WHERE Status = @status";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@status", 5);
            SqlDataReader dr = cmd.ExecuteReader();

            while(dr.Read())
            {
                registationNumbers.Add(dr["RegistrationNumber"].ToString());
            }

            dr.Close();

            return registationNumbers;
            
        }
        public static bool AddStudent(Student studentToBeStored)
        {
            if (DoesStudentExist(studentToBeStored.registrationNumber))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName, @LastName, @Contact, @Email, @RegistrationNumber,  (SELECT LookupId From Lookup WHERE Name = @status AND Category = 'STUDENT_STATUS'));", con);
            cmd.Parameters.AddWithValue("@FirstName", studentToBeStored.firstName);
            cmd.Parameters.AddWithValue("@LastName", studentToBeStored.lastName);
            cmd.Parameters.AddWithValue("@Contact", studentToBeStored.contactNumber);
            cmd.Parameters.AddWithValue("@Email", studentToBeStored.email);
            cmd.Parameters.AddWithValue("@RegistrationNumber", studentToBeStored.registrationNumber);
            cmd.Parameters.AddWithValue("@Status", studentToBeStored.status);

            cmd.ExecuteNonQuery();

            return true;
        }
        public static bool DeleteStudent(string registrationNumber)
        {
            if (!DoesStudentExist(registrationNumber))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM Student WHERE RegistrationNumber = @registrationNumber;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@registrationNumber", registrationNumber);
            cmd.ExecuteNonQuery();

            return true;
        }
        public static DataTable GetAllStudentsTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT FirstName, LastName, Contact, Email, RegistrationNumber, (Select Name FROM Lookup WHERE LookupId = Status) AS Status FROM Student;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesStudentExist(string registrationNumber)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM Student WHERE RegistrationNumber = @registrationNumber;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue ("@registrationNumber", registrationNumber);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateStudentInDatabase(Student studentToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE Student SET FirstName = @firstName, LastName = @lastName, Contact = @contact, Email = @email, RegistrationNumber = @registrationNumber, Status = (SELECT LookupId From Lookup WHERE Name = @status AND Category = 'STUDENT_STATUS') WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand (query, con);
            cmd.Parameters.AddWithValue("@id", studentToBeUpdated.databaseID);
            cmd.Parameters.AddWithValue("@firstName", studentToBeUpdated.firstName);
            cmd.Parameters.AddWithValue("@lastName", studentToBeUpdated.lastName);
            cmd.Parameters.AddWithValue("@contact", studentToBeUpdated.contactNumber);
            cmd.Parameters.AddWithValue("@email", studentToBeUpdated.email);
            cmd.Parameters.AddWithValue("@registrationNumber", studentToBeUpdated.registrationNumber);
            cmd.Parameters.AddWithValue("@status", studentToBeUpdated.status);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
        public static int GetAttendanceDatabaseID(DateTime date)
        {
            /*
             * Searches for attendance ID in database and creates a new
             * entry if date doesn't already exist in the table
             */

            int databaseID;

            var con = Configuration.getInstance().getConnection();

            string selectQuery = "SELECT Id FROM ClassAttendance WHERE AttendanceDate = @date";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@date", date.Date);
            SqlDataReader dr = selectCommand.ExecuteReader();

            if (dr.Read())
            {
                databaseID = Convert.ToInt32(dr["Id"]);
                dr.Close();
            }

            else
            {
                dr.Close();
                string insertQuery = "INSERT INTO ClassAttendance VALUES (@date); SELECT SCOPE_IDENTITY();";
                SqlCommand insertCommand = new SqlCommand(insertQuery, con);
                insertCommand.Parameters.AddWithValue("@date", date.Date);
                databaseID = Convert.ToInt32(insertCommand.ExecuteScalar());
            }

            return databaseID;
        }
        public static bool DoesStudentAttendanceExist(string registrationNumber, int attendanceDatbaseID)
        {
            Student student = StudentCRUD.GetStudent(registrationNumber);

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM StudentAttendance WHERE AttendanceId = @attendanceId AND StudentId = @studentId";
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.Parameters.AddWithValue("@attendanceId", attendanceDatbaseID);
            cmd.Parameters.AddWithValue("@studentId", student.databaseID);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool MarkAttendance(DateTime date, string registrationNumber, string status)
        {
            int attendanceDatabaseID = StudentCRUD.GetAttendanceDatabaseID(date);
            int studentDatabaseID = StudentCRUD.GetStudentDatabaseID(registrationNumber);

            var con = Configuration.getInstance().getConnection();

            if (!StudentCRUD.DoesStudentAttendanceExist(registrationNumber, attendanceDatabaseID))
            {
                string query = "INSERT INTO StudentAttendance VALUES (@attendanceId, @studentId, (SELECT LookupId FROM Lookup WHERE Name = @status AND Category = 'ATTENDANCE_STATUS'));";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@attendanceId", attendanceDatabaseID);

                cmd.Parameters.AddWithValue("@studentId", studentDatabaseID);

                cmd.Parameters.AddWithValue("@status", status);

                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected > 0;
            }
            else
            {
                string query = "UPDATE StudentAttendance SET AttendanceStatus = (SELECT LookupId FROM Lookup WHERE Name = @status AND Category = 'ATTENDANCE_STATUS') WHERE AttendanceId = @attendanceId AND StudentId = @studentId;";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@attendanceId", attendanceDatabaseID);

                cmd.Parameters.AddWithValue("@studentId", studentDatabaseID);

                cmd.Parameters.AddWithValue("@status", status);

                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected > 0;
            }

            /*int databaseStatusId = 0;

            if (status == "Present")
            {
                databaseStatusId = 1;
            }
            else if (status == "Absent")
            {
                databaseStatusId = 2;
            }
            else if (status == "Leave")
            {
                databaseStatusId = 3;
            }
            else if (status == "Late")
            {
                databaseStatusId = 4;
            }*/
        }
    }
}

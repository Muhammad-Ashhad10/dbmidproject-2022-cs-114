﻿using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.DL
{
    class RubricLevelCRUD
    {
        public static Rubric_Level GetRubricLevel(int id)
        {
            Rubric_Level requiredRubricLevel;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, RubricId, Details, MeasurementLevel FROM RubricLevel WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredRubricLevel = new Rubric_Level();
                requiredRubricLevel.databaseID = int.Parse(dr["Id"].ToString());
                requiredRubricLevel.details = dr["Details"].ToString();
                requiredRubricLevel.measurementLevel = int.Parse(dr["MeasurementLevel"].ToString());

                int rubricDatabaseID = int.Parse(dr["RubricId"].ToString());
                requiredRubricLevel.rubric = RubricCRUD.GetRubric(rubricDatabaseID);
            }
            else
            {
                requiredRubricLevel = null;
            }

            dr.Close();

            return requiredRubricLevel;
        }
        public static List<string> GetAllRubricLevelIDs()
        {
            List<string> rubricLevels = new List<string>();

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id FROM RubricLevel";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                rubricLevels.Add(dr["Id"].ToString());
            }

            dr.Close();

            return rubricLevels;
        }
        public static bool AddRubricLevel(Rubric_Level rubricLevelToBeStored)
        {
            if (DoesRubricLevelExist(rubricLevelToBeStored.databaseID))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@RubricId, @Details, @MeasurementLevel);", con);
            cmd.Parameters.AddWithValue("@Details", rubricLevelToBeStored.details);
            cmd.Parameters.AddWithValue("@MeasurementLevel", rubricLevelToBeStored.measurementLevel);
            cmd.Parameters.AddWithValue("@RubricId", rubricLevelToBeStored.rubric.databaseID);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static bool DeleteRubricLevel(int id)
        {
            if (!DoesRubricLevelExist(id))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM RubricLevel WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static DataTable GetAllRubricLevelsTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT Id, RubricId, Details, MeasurementLevel FROM RubricLevel;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesRubricLevelExist(int id)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM RubricLevel WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateRubricLevelInDatabase(Rubric_Level rubricLevelToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE RubricLevel SET RubricId = @rubricId, Details = @details, MeasurementLevel = @measurementLevel WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@details", rubricLevelToBeUpdated.details);
            cmd.Parameters.AddWithValue("@rubricId", rubricLevelToBeUpdated.rubric.databaseID);
            cmd.Parameters.AddWithValue("@id", rubricLevelToBeUpdated.databaseID);
            cmd.Parameters.AddWithValue("@measurementLevel", rubricLevelToBeUpdated.measurementLevel);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
    }
}
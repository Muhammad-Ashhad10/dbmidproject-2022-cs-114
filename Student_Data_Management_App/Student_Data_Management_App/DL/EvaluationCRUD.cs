﻿using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.DL
{
    class EvaluationCRUD
    {
        public static Evaluation GetEvaluation(string studentRegistrationNumber)
        {
            Evaluation requiredEvaluation;

            int studentDatabaseID = StudentCRUD.GetStudentDatabaseID(studentRegistrationNumber);

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT AssessmentComponentId, RubricMeasurementId, EvaluationDate FROM StudentResult WHERE StudentId = @studentId";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@StudentId", studentDatabaseID);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredEvaluation = new Evaluation();
                requiredEvaluation.student = StudentCRUD.GetStudent(studentRegistrationNumber);
                
                int assessmentComponentId = int.Parse(dr["AssessmentComponentId"].ToString());
                requiredEvaluation.assessmentComponent = AssessmentComponentCRUD.GetAssessmentComponentByDatabaseID(assessmentComponentId);

                int rubricMeasurementDatabaseID = int.Parse(dr["RubricMeasurementId"].ToString());
                requiredEvaluation.rubricMeasurement = RubricLevelCRUD.GetRubricLevel(rubricMeasurementDatabaseID);

                requiredEvaluation.evaluationDate = DateTime.Parse(dr["EvaluationDate"].ToString());
            }
            else
            {
                requiredEvaluation = null;
            }

            dr.Close();

            return requiredEvaluation;
        }
        public static bool AddEvaluation(Assessment_Component assessmentComponentToBeStored)
        {
            if (DoesAssessmentComponentExist(assessmentComponentToBeStored.name))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name, @RubricId, @TotalMarks, @DateCreated, @DateUpdated, @AssessmentId);", con);
            cmd.Parameters.AddWithValue("@Name", assessmentComponentToBeStored.name);
            cmd.Parameters.AddWithValue("@RubricId", assessmentComponentToBeStored.rubric.databaseID);
            cmd.Parameters.AddWithValue("@TotalMarks", assessmentComponentToBeStored.totalMarks);
            cmd.Parameters.AddWithValue("@DateCreated", assessmentComponentToBeStored.dateCreated);
            cmd.Parameters.AddWithValue("@DateUpdated", assessmentComponentToBeStored.dateUpdated);
            cmd.Parameters.AddWithValue("@AssessmentId", assessmentComponentToBeStored.assessment.databaseId);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static bool DeleteAssessmentComponent(string name)
        {
            if (!DoesAssessmentComponentExist(name))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM AssessmentComponent WHERE Name = @name;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static DataTable GetAllAssessmentComponentsTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT Id, Name, RubricId, TotalMarks, DateCreated, DateUpdated, AssessmentId FROM AssessmentComponents;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesAssessmentComponentExist(string name)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM AssessmentComponent WHERE Name = @name;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateAssessmentComponentInDatabase(Assessment_Component assessmentComponentToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE AssessmentComponent SET Name = @name, RubricId = @rubricId, TotalMarks = @totalMarks, DateCreated = @dateCreated, DateUpdated = @dateUpdated, AssessmentId = @assessmentId WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", assessmentComponentToBeUpdated.databaseId);
            cmd.Parameters.AddWithValue("@name", assessmentComponentToBeUpdated.name);
            cmd.Parameters.AddWithValue("@rubricId", assessmentComponentToBeUpdated.rubric.databaseID);
            cmd.Parameters.AddWithValue("@totalMarks", assessmentComponentToBeUpdated.totalMarks);
            cmd.Parameters.AddWithValue("@dateCreated", assessmentComponentToBeUpdated.dateCreated);
            cmd.Parameters.AddWithValue("@dateUpdated", assessmentComponentToBeUpdated.dateUpdated);
            cmd.Parameters.AddWithValue("@assessmentId", assessmentComponentToBeUpdated.assessment.databaseId);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
    }
}

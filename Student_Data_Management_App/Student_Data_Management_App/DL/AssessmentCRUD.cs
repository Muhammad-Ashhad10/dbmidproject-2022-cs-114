﻿using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.DL
{
    class AssessmentCRUD
    {
        public static Assessment GetAssessment(string title)
        {
            Assessment requiredAssessment;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Title, DateCreated, TotalMarks, TotalWeightage FROM Assessment WHERE Title = @title";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@title", title);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredAssessment = new Assessment();
                requiredAssessment.databaseId = int.Parse(dr["Id"].ToString());
                requiredAssessment.title = dr["Title"].ToString();
                requiredAssessment.dateCreated = DateTime.Parse(dr["DateCreated"].ToString());
                requiredAssessment.totalMarks = int.Parse(dr["TotalMarks"].ToString());
                requiredAssessment.totalWeightage = int.Parse(dr["TotalWeightage"].ToString());
            }
            else
            {
                requiredAssessment = null;
            }

            dr.Close();

            return requiredAssessment;
        }
        public static Assessment GetAssessmentByDatabaseID(int databaseID)
        {
            Assessment requiredAssessment;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Title, DateCreated, TotalMarks, TotalWeightage FROM Assessment WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", databaseID);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredAssessment = new Assessment();
                requiredAssessment.databaseId = int.Parse(dr["Id"].ToString());
                requiredAssessment.title = dr["Title"].ToString();
                requiredAssessment.dateCreated = DateTime.Parse(dr["DateCreated"].ToString());
                requiredAssessment.totalMarks = int.Parse(dr["TotalMarks"].ToString());
                requiredAssessment.totalWeightage = int.Parse(dr["TotalWeightage"].ToString());
            }
            else
            {
                requiredAssessment = null;
            }

            dr.Close();

            return requiredAssessment;
        }
        public static int GetAssessmentDatabaseID(string title)
        {
            int databaseID = -1;

            var con = Configuration.getInstance().getConnection();

            string selectQuery = "SELECT Id FROM Assessment WHERE Title = @title";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@title", title);
            SqlDataReader dr = selectCommand.ExecuteReader();

            if (dr.Read())
            {
                databaseID = Convert.ToInt32(dr["Id"]);
            }

            dr.Close();

            return databaseID;
        }
        public static List<string> GetAllAssessmentTitles()
        {
            List<string> assessments = new List<string>();

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Title FROM Assessment";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                assessments.Add(dr["Assessment"].ToString());
            }

            dr.Close();

            return assessments;
        }
        public static bool AddAssessment(Assessment assessmentToBeStored)
        {
            if (DoesAssessmentExist(assessmentToBeStored.title))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title, @DateCreated, @TotalMarks, @TotalWeightage);", con);
            cmd.Parameters.AddWithValue("@Name", assessmentToBeStored.title);
            cmd.Parameters.AddWithValue("@DateCreated", assessmentToBeStored.dateCreated);
            cmd.Parameters.AddWithValue("@TotalMarks", assessmentToBeStored.totalMarks);
            cmd.Parameters.AddWithValue("@TotalWeightage", assessmentToBeStored.totalWeightage);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static bool DeleteAssessment(string title)
        {
            if (!DoesAssessmentExist(title))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM Assessment WHERE Title = @title;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@title", title);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static DataTable GetAllAssessmentsTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT Title, DateCreated, TotalMarks, TotalWeightage FROM Assessment;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesAssessmentExist(string title)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM Assessment WHERE Title = @title;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@title", title);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateAssessmentInDatabase(Assessment assessmentToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE Clo SET Title = @title, DateCreated = @dateCreated, TotalMarks = @totalMarks, TotalWeightage = @totalWeightage WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", assessmentToBeUpdated.databaseId);
            cmd.Parameters.AddWithValue("@title", assessmentToBeUpdated.title);
            cmd.Parameters.AddWithValue("@dateCreated", assessmentToBeUpdated.dateCreated);
            cmd.Parameters.AddWithValue("@totalMarks", assessmentToBeUpdated.totalMarks);
            cmd.Parameters.AddWithValue("@totalWeightage", assessmentToBeUpdated.totalWeightage);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
    }
}

﻿using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Student_Data_Management_App.DL
{
    class AssessmentComponentCRUD
    {
        public static Assessment_Component GetAssessmentComponent(string name)
        {
            Assessment_Component requiredAssessmentComponent;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Name, RubricId, TotalMarks, DateCreated, DateUpdated, AssessmentId FROM RubricLevel WHERE Name = @name";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", name);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredAssessmentComponent = new Assessment_Component();
                requiredAssessmentComponent.databaseId = int.Parse(dr["Id"].ToString());
                requiredAssessmentComponent.name = dr["Name"].ToString();

                int rubricDatabaseID = int.Parse(dr["RubricId"].ToString());
                requiredAssessmentComponent.rubric = RubricCRUD.GetRubric(rubricDatabaseID);

                requiredAssessmentComponent.totalMarks = int.Parse(dr["TotalMarks"].ToString());
                requiredAssessmentComponent.dateCreated = DateTime.Parse(dr["DateCreated"].ToString());
                requiredAssessmentComponent.dateUpdated = DateTime.Parse(dr["DateUpdated"].ToString());


                int assessmentDatabaseID = int.Parse(dr["RubricId"].ToString());
                requiredAssessmentComponent.assessment = AssessmentCRUD.GetAssessmentByDatabaseID(assessmentDatabaseID);
            }
            else
            {
                requiredAssessmentComponent = null;
            }

            dr.Close();

            return requiredAssessmentComponent;
        }
        public static Assessment_Component GetAssessmentComponentByDatabaseID(int id)
        {
            Assessment_Component requiredAssessmentComponent;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Name, RubricId, TotalMarks, DateCreated, DateUpdated, AssessmentId FROM RubricLevel WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredAssessmentComponent = new Assessment_Component();
                requiredAssessmentComponent.databaseId = int.Parse(dr["Id"].ToString());
                requiredAssessmentComponent.name = dr["Name"].ToString();

                int rubricDatabaseID = int.Parse(dr["RubricId"].ToString());
                requiredAssessmentComponent.rubric = RubricCRUD.GetRubric(rubricDatabaseID);

                requiredAssessmentComponent.totalMarks = int.Parse(dr["TotalMarks"].ToString());
                requiredAssessmentComponent.dateCreated = DateTime.Parse(dr["DateCreated"].ToString());
                requiredAssessmentComponent.dateUpdated = DateTime.Parse(dr["DateUpdated"].ToString());


                int assessmentDatabaseID = int.Parse(dr["RubricId"].ToString());
                requiredAssessmentComponent.assessment = AssessmentCRUD.GetAssessmentByDatabaseID(assessmentDatabaseID);
            }
            else
            {
                requiredAssessmentComponent = null;
            }

            dr.Close();

            return requiredAssessmentComponent;
        }
        public static List<string> GetAllAssessmentComponentTitles()
        {
            List<string> assessmentComponents = new List<string>();

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Title FROM AssessmentComponent";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                assessmentComponents.Add(dr["Id"].ToString());
            }

            dr.Close();

            return assessmentComponents;
        }
        public static bool AddAssessmentComponent(Assessment_Component assessmentComponentToBeStored)
        {
            if (DoesAssessmentComponentExist(assessmentComponentToBeStored.name))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name, @RubricId, @TotalMarks, @DateCreated, @DateUpdated, @AssessmentId);", con);
            cmd.Parameters.AddWithValue("@Name", assessmentComponentToBeStored.name);
            cmd.Parameters.AddWithValue("@RubricId", assessmentComponentToBeStored.rubric.databaseID);
            cmd.Parameters.AddWithValue("@TotalMarks", assessmentComponentToBeStored.totalMarks);
            cmd.Parameters.AddWithValue("@DateCreated", assessmentComponentToBeStored.dateCreated);
            cmd.Parameters.AddWithValue("@DateUpdated", assessmentComponentToBeStored.dateUpdated);
            cmd.Parameters.AddWithValue("@AssessmentId", assessmentComponentToBeStored.assessment.databaseId);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static bool DeleteAssessmentComponent(string name)
        {
            if (!DoesAssessmentComponentExist(name))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM AssessmentComponent WHERE Name = @name;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static DataTable GetAllAssessmentComponentsTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT Id, Name, RubricId, TotalMarks, DateCreated, DateUpdated, AssessmentId FROM AssessmentComponents;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesAssessmentComponentExist(string name)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM AssessmentComponent WHERE Name = @name;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateAssessmentComponentInDatabase(Assessment_Component assessmentComponentToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE AssessmentComponent SET Name = @name, RubricId = @rubricId, TotalMarks = @totalMarks, DateCreated = @dateCreated, DateUpdated = @dateUpdated, AssessmentId = @assessmentId WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", assessmentComponentToBeUpdated.databaseId);
            cmd.Parameters.AddWithValue("@name", assessmentComponentToBeUpdated.name);
            cmd.Parameters.AddWithValue("@rubricId", assessmentComponentToBeUpdated.rubric.databaseID);
            cmd.Parameters.AddWithValue("@totalMarks", assessmentComponentToBeUpdated.totalMarks);
            cmd.Parameters.AddWithValue("@dateCreated", assessmentComponentToBeUpdated.dateCreated);
            cmd.Parameters.AddWithValue("@dateUpdated", assessmentComponentToBeUpdated.dateUpdated);
            cmd.Parameters.AddWithValue("@assessmentId", assessmentComponentToBeUpdated.assessment.databaseId);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
    }
}

﻿using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Data_Management_App.DL
{
    class RubricCRUD
    {
        public static Rubric GetRubric(int id)
        {
            Rubric requiredRubric;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Details, CloId FROM Rubric WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredRubric = new Rubric();
                requiredRubric.databaseID = int.Parse(dr["Id"].ToString());
                requiredRubric.details = dr["Details"].ToString();

                int cloDatabaseID = int.Parse(dr["CloId"].ToString());

                requiredRubric.clo = CloCRUD.GetCLOByDatabaseID(cloDatabaseID);
            }
            else
            {
                requiredRubric = null;
            }

            dr.Close();

            return requiredRubric;
        }
        public static List<string> GetAllRubricIDs()
        {
            List<string> rubrics = new List<string>();

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id FROM Rubric";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                rubrics.Add(dr["Id"].ToString());
            }

            dr.Close();

            return rubrics;
        }
        public static bool AddRubric(Rubric rubricToBeStored)
        {
            if (DoesRubricExist(rubricToBeStored.databaseID))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Rubric values (@Details, @CloId);", con);
            cmd.Parameters.AddWithValue("@Details", rubricToBeStored.details);
            cmd.Parameters.AddWithValue("@CloId", rubricToBeStored.clo.databaseID);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static bool DeleteRubric(int id)
        {
            if (!DoesRubricExist(id))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM Rubric WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static DataTable GetAllRubricsTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT Id, Details, CloId FROM Rubric;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesRubricExist(int id)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM Rubric WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", id);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateRubricInDatabase(Rubric rubricToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE Rubric SET Details = @details, CloId = @cloId WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@details", rubricToBeUpdated.details);
            cmd.Parameters.AddWithValue("@cloId", rubricToBeUpdated.clo.databaseID);
            cmd.Parameters.AddWithValue("@id", rubricToBeUpdated.databaseID);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
    }
}

﻿using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Student_Data_Management_App.DL
{
    class CloCRUD
    {
        public static CLO GetCLO(string name)
        {
            CLO requiredCLO;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Name, DateCreated, DateUpdated FROM Clo WHERE Name = @name";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredCLO = new CLO();
                requiredCLO.databaseID = int.Parse(dr["Id"].ToString());
                requiredCLO.name = dr["Name"].ToString();
                requiredCLO.dateCreated = DateTime.Parse(dr["DateCreated"].ToString());
                requiredCLO.dateUpdated = DateTime.Parse(dr["DateUpdated"].ToString());
            }
            else
            {
                requiredCLO = null;
            }

            dr.Close();

            return requiredCLO;
        }
        public static CLO GetCLOByDatabaseID(int databaseID)
        {
            CLO requiredCLO;

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Id, Name, DateCreated, DateUpdated FROM Clo WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", databaseID);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                requiredCLO = new CLO();
                requiredCLO.databaseID = int.Parse(dr["Id"].ToString());
                requiredCLO.name = dr["Name"].ToString();
                requiredCLO.dateCreated = DateTime.Parse(dr["DateCreated"].ToString());
                requiredCLO.dateUpdated = DateTime.Parse(dr["DateUpdated"].ToString());
            }
            else
            {
                requiredCLO = null;
            }

            dr.Close();

            return requiredCLO;
        }
        public static int GetCLODatabaseID(string name)
        {
            int databaseID = -1;

            var con = Configuration.getInstance().getConnection();

            string selectQuery = "SELECT Id FROM Clo WHERE Name = @name";
            SqlCommand selectCommand = new SqlCommand(selectQuery, con);
            selectCommand.Parameters.AddWithValue("@name", name);
            SqlDataReader dr = selectCommand.ExecuteReader();

            if (dr.Read())
            {
                databaseID = Convert.ToInt32(dr["Id"]);
            }

            dr.Close();

            return databaseID;
        }
        public static List<string> GetAllCLONames()
        {
            List<string> clos = new List<string> ();

            var con = Configuration.getInstance().getConnection();

            string query = "SELECT Name FROM Clo";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                clos.Add(dr["Name"].ToString());
            }

            dr.Close();

            return clos;
        }
        public static bool AddClo(CLO cloToBeStored)
        {
            if (DoesCloExist(cloToBeStored.name))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Clo values (@Name, @DateCreated, @DateUpdated);", con);
            cmd.Parameters.AddWithValue("@Name", cloToBeStored.name);
            cmd.Parameters.AddWithValue("@DateCreated", cloToBeStored.dateCreated);
            cmd.Parameters.AddWithValue("@DateUpdated", cloToBeStored.dateUpdated);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static bool DeleteClo(string name)
        {
            if (!DoesCloExist(name))
            {
                return false;
            }

            var con = Configuration.getInstance().getConnection();
            string query = "DELETE FROM Clo WHERE Name = @name;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);

            int count = cmd.ExecuteNonQuery();

            return count > 0;
        }
        public static DataTable GetAllClosTable()
        {
            var con = Configuration.getInstance().getConnection();
            string query = "SELECT Name, DateCreated, DateUpdated FROM Clo;";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
        public static bool DoesCloExist(string name)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "SELECT COUNT(*) FROM Clo WHERE Name = @name;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@name", name);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            return count > 0;
        }
        public static bool UpdateCloInDatabase(CLO cloToBeUpdated)
        {
            var con = Configuration.getInstance().getConnection();

            string query = "UPDATE Clo SET Name = @name, DateCreated = @dateCreated, DateUpdated = @dateUpdated WHERE Id = @id;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", cloToBeUpdated.databaseID);
            cmd.Parameters.AddWithValue("@name", cloToBeUpdated.name);
            cmd.Parameters.AddWithValue("@dateCreated", cloToBeUpdated.dateCreated);
            cmd.Parameters.AddWithValue("@dateUpdated", cloToBeUpdated.dateUpdated);

            int rowsAffected = cmd.ExecuteNonQuery();

            return rowsAffected > 0;
        }
    }
}

﻿using Student_Data_Management_App.DL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class Mark_Attendance_Form : Form
    {
        public Mark_Attendance_Form()
        {
            InitializeComponent();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            Form mainMenuForm = new Main_Menu();
            mainMenuForm.Show();
            this.Close();
        }

        private void Mark_Attendance_Form_Load(object sender, EventArgs e)
        {
            
        }

        private void markButton_Click(object sender, EventArgs e)
        {
            Form attendanceForm = new Mark_Attendance_Form_2(dateTimePicker.Value);
            attendanceForm.Show();
            this.Close();
        }
    }
}

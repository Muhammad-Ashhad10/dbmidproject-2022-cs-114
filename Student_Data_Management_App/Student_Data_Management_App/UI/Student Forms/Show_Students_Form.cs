﻿using Student_Data_Management_App.DL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class Show_Students_Form : Form
    {
        public Show_Students_Form()
        {
            InitializeComponent();
        }

        private void Show_Students_Form_Load(object sender, EventArgs e)
        {
            DataTable dt = StudentCRUD.GetAllStudentsTable();
            dataGridView1.DataSource = dt;
        }
    }
}

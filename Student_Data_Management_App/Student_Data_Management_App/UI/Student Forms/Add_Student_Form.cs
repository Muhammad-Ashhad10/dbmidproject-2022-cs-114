﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using Student_Data_Management_App.BL;
using Student_Data_Management_App.DL;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class Add_Student_Form : Form
    {
        public Add_Student_Form()
        {
            InitializeComponent();
        }

        /*
         * This method creates a Student object and passes it on to
         * the Data Layer AddStudent() method to be added to the database
         */
        private void submitButton_Click(object sender, EventArgs e)
        {
            Student studentToBeAdded = new Student(firstName.Text, lastName.Text, contact.Text, email.Text, registrationNumber.Text, statusBox.Text);

            bool isStudentAdded = StudentCRUD.AddStudent(studentToBeAdded);

            if (isStudentAdded)
            {
                MessageBox.Show("Successfully saved");
            }
            else
            {
                MessageBox.Show($"Student already exists: { studentToBeAdded.registrationNumber }");
            }
        }

        private void Add_Student_Form_Load(object sender, EventArgs e)
        {

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            Form mainMenu = new Main_Menu();
            mainMenu.Show();
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form deleteStudentForm = new Delete_Student_Form();
            deleteStudentForm.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form viewStudentsForm = new Show_Students_Form();
            viewStudentsForm.Show();
            this.Close();
        }
    }
}

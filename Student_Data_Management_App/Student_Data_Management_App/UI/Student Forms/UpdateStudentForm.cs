﻿using Student_Data_Management_App.BL;
using Student_Data_Management_App.DL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class UpdateStudentForm : Form
    {
        public UpdateStudentForm()
        {
            InitializeComponent();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (!registrationNumber.ReadOnly)
            {
                Student requiredStudent = StudentCRUD.GetStudent(registrationNumber.Text);

                if (requiredStudent != null)
                {
                    registrationNumber.ReadOnly = true;
                    registrationNumberDisplay.ReadOnly = false;
                    firstName.ReadOnly = false;
                    lastName.ReadOnly = false;
                    contact.ReadOnly = false;
                    email.ReadOnly = false;

                    ID.Text = requiredStudent.databaseID.ToString();
                    registrationNumberDisplay.Text = requiredStudent.registrationNumber;
                    firstName.Text = requiredStudent.firstName;
                    lastName.Text = requiredStudent.lastName;
                    contact.Text = requiredStudent.contactNumber;
                    email.Text = requiredStudent.email;
                    status.Text = requiredStudent.status;
                }
                else
                {
                    MessageBox.Show($"No data found: {registrationNumber.Text}");
                }
            }
            else
            {
                registrationNumber.ReadOnly = false;
                registrationNumberDisplay.ReadOnly = true;
                firstName.ReadOnly = true;
                lastName.ReadOnly = true;
                contact.ReadOnly = true;
                email.ReadOnly = true;

                ID.Clear();
                registrationNumberDisplay.Clear();
                firstName.Clear();
                lastName.Clear();
                contact.Clear();
                email.Clear();
                status.Text = "";
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            Form studentMainMenuForm = new Student_Main_Menu();
            studentMainMenuForm.Show();
            this.Close();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (registrationNumber.ReadOnly)
            {
                Student requiredStudent = new Student();

                requiredStudent.databaseID = int.Parse(ID.Text);
                requiredStudent.firstName = firstName.Text;
                requiredStudent.lastName = lastName.Text;
                requiredStudent.email = email.Text;
                requiredStudent.contactNumber = contact.Text;
                requiredStudent.status = status.Text;
                requiredStudent.registrationNumber = registrationNumberDisplay.Text;

                bool isUpdated = StudentCRUD.UpdateStudentInDatabase(requiredStudent);

                if (isUpdated)
                {
                    MessageBox.Show("Student Successfully Updated");
                }
            }
        }
    }
}

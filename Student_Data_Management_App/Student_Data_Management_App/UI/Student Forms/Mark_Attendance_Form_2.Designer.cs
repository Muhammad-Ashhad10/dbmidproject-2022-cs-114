﻿namespace Student_Data_Management_App.UI.Student_Forms
{
    partial class Mark_Attendance_Form_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mark_Attendance_Form_2));
            this.markAttendanceButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.Manage_Students_Button = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.registrationNumber = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.ComboBox();
            this.backButton = new System.Windows.Forms.Button();
            this.markButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // markAttendanceButton
            // 
            this.markAttendanceButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.markAttendanceButton.FlatAppearance.BorderSize = 0;
            this.markAttendanceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.markAttendanceButton.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markAttendanceButton.ForeColor = System.Drawing.Color.White;
            this.markAttendanceButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.markAttendanceButton.Location = new System.Drawing.Point(14, 604);
            this.markAttendanceButton.Name = "markAttendanceButton";
            this.markAttendanceButton.Size = new System.Drawing.Size(239, 94);
            this.markAttendanceButton.TabIndex = 14;
            this.markAttendanceButton.Text = "Mark Attendance";
            this.markAttendanceButton.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(14, 106);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(239, 94);
            this.button2.TabIndex = 10;
            this.button2.Text = "Add Student";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(94)))));
            this.panel1.Controls.Add(this.markAttendanceButton);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.Manage_Students_Button);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(253, 794);
            this.panel1.TabIndex = 84;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(3, 602);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 96);
            this.panel3.TabIndex = 10;
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(14, 234);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(239, 94);
            this.button5.TabIndex = 13;
            this.button5.Text = "Remove Student";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // Manage_Students_Button
            // 
            this.Manage_Students_Button.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Manage_Students_Button.FlatAppearance.BorderSize = 0;
            this.Manage_Students_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Manage_Students_Button.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Manage_Students_Button.ForeColor = System.Drawing.Color.White;
            this.Manage_Students_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Manage_Students_Button.Location = new System.Drawing.Point(14, 504);
            this.Manage_Students_Button.Name = "Manage_Students_Button";
            this.Manage_Students_Button.Size = new System.Drawing.Size(239, 94);
            this.Manage_Students_Button.TabIndex = 1;
            this.Manage_Students_Button.Text = "Update Student";
            this.Manage_Students_Button.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(14, 366);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(239, 94);
            this.button3.TabIndex = 11;
            this.button3.Text = "View Students";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(94)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // registrationNumber
            // 
            this.registrationNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.registrationNumber.FormattingEnabled = true;
            this.registrationNumber.Location = new System.Drawing.Point(613, 158);
            this.registrationNumber.Name = "registrationNumber";
            this.registrationNumber.Size = new System.Drawing.Size(279, 28);
            this.registrationNumber.TabIndex = 91;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(378, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 24);
            this.label1.TabIndex = 89;
            this.label1.Text = "Registration Number:";
            // 
            // status
            // 
            this.status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.status.FormattingEnabled = true;
            this.status.Items.AddRange(new object[] {
            "Present",
            "Absent",
            "Leave",
            "Late"});
            this.status.Location = new System.Drawing.Point(613, 366);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(279, 28);
            this.status.TabIndex = 90;
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(94)))));
            this.backButton.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.ForeColor = System.Drawing.Color.White;
            this.backButton.Location = new System.Drawing.Point(408, 569);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(120, 38);
            this.backButton.TabIndex = 88;
            this.backButton.Text = "Main Menu";
            this.backButton.UseVisualStyleBackColor = false;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // markButton
            // 
            this.markButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(94)))));
            this.markButton.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markButton.ForeColor = System.Drawing.Color.White;
            this.markButton.Location = new System.Drawing.Point(931, 569);
            this.markButton.Name = "markButton";
            this.markButton.Size = new System.Drawing.Size(99, 38);
            this.markButton.TabIndex = 87;
            this.markButton.Text = "Mark";
            this.markButton.UseVisualStyleBackColor = false;
            this.markButton.Click += new System.EventHandler(this.markButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Rubik", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(492, 370);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 24);
            this.label4.TabIndex = 86;
            this.label4.Text = "Status:";
            // 
            // Mark_Attendance_Form_2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 794);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.registrationNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.status);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.markButton);
            this.Controls.Add(this.label4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1200, 850);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1200, 850);
            this.Name = "Mark_Attendance_Form_2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mark_Attendance_Form_2";
            this.Load += new System.EventHandler(this.Mark_Attendance_Form_2_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button markAttendanceButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button Manage_Students_Button;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox registrationNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox status;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button markButton;
        private System.Windows.Forms.Label label4;
    }
}
﻿using Student_Data_Management_App.DL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class Mark_Attendance_Form_2 : Form
    {
        public DateTime attendanceDate { get; set; }
        public Mark_Attendance_Form_2(DateTime AttendanceDate)
        {
            InitializeComponent();
            this.attendanceDate = AttendanceDate;
        }

        private void markButton_Click(object sender, EventArgs e)
        {
            bool isMarked = StudentCRUD.MarkAttendance(this.attendanceDate, registrationNumber.Text, status.Text);
            if (isMarked)
            {
                MessageBox.Show($"Marked/Updated Attendance for { registrationNumber.Text }");
            }
            else
            {
                MessageBox.Show("Error: Could not mark attendance");
            }
        }

        private void Mark_Attendance_Form_2_Load(object sender, EventArgs e)
        {
            try
            {
                List<string> students = StudentCRUD.GetAllActiveRegistrationNumbers();

                // Clear existing items in the ComboBox
                registrationNumber.Items.Clear();

                // Add the new items from the list
                registrationNumber.Items.AddRange(students.ToArray());
            }
            catch (Exception ex)
            {
                // Handle exceptions or log them for debugging
                MessageBox.Show($"Error loading student registration numbers: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            Form mainMenu = new Main_Menu();
            mainMenu.Show();
            this.Close();
        }
    }
}

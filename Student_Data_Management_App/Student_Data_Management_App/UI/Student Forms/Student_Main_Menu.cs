﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Student_Data_Management_App.UI.CLO_Forms;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class Student_Main_Menu : Form
    {
        public Student_Main_Menu()
        {
            InitializeComponent();
        }

        private void Student_Main_Menu_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form addStudentForm = new Add_Student_Form();
            addStudentForm.Show();
            this.Close();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            Form mainMenu = new Main_Menu();
            mainMenu.Show();
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form deleteStudentForm = new Delete_Student_Form();
            deleteStudentForm.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form viewStudentsForm = new Show_Students_Form();
            viewStudentsForm.Show();
            this.Close();
        }

        private void Manage_Students_Button_Click(object sender, EventArgs e)
        {
            Form updateStudentForm = new UpdateStudentForm();
            updateStudentForm.Show();
            this.Close();
        }

        private void markAttendanceButton_Click(object sender, EventArgs e)
        {
            Form markAttendanceForm = new Mark_Attendance_Form();
            markAttendanceForm.Show();
            this.Close();
        }
    }
}

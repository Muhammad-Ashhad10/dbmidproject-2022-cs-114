﻿using Student_Data_Management_App.DL;
using Student_Data_Management_App.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Student_Data_Management_App.UI.Student_Forms
{
    public partial class Delete_Student_Form : Form
    {
        public Delete_Student_Form()
        {
            InitializeComponent();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void firstName_TextChanged(object sender, EventArgs e)
        {

        }
        private void label3_Click(object sender, EventArgs e)
        {

        }
        private void lastName_TextChanged(object sender, EventArgs e)
        {

        }
        private void label6_Click(object sender, EventArgs e)
        {

        }
        private void contact_TextChanged(object sender, EventArgs e)
        {

        }
        private void label5_Click(object sender, EventArgs e)
        {

        }
        private void email_TextChanged(object sender, EventArgs e)
        {

        }
        private void label7_Click(object sender, EventArgs e)
        {

        }
        private void registrationNumber_TextChanged(object sender, EventArgs e)
        {

        }
        private void label4_Click(object sender, EventArgs e)
        {

        }
        private void Delete_Student_Form_Load(object sender, EventArgs e)
        {

        }
        private void statusBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void backButton_Click(object sender, EventArgs e)
        {
            Form mainMenu = new Main_Menu();
            mainMenu.Show();
            this.Close();
        }
        private void submitButton_Click(object sender, EventArgs e)
        {
            if (StudentCRUD.DeleteStudent(registrationNumber.Text))
            {
                MessageBox.Show($"{ registrationNumber.Text }: Student Successfully deleted");
                registrationNumber.Clear();
                firstName.Clear();
                lastName.Clear();
                contact.Clear();
                status.Clear();
                email.Clear();
            }
            else
            {
                MessageBox.Show($"No data found: {registrationNumber.Text}");
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form addStudentForm = new Add_Student_Form();
            addStudentForm.Show();
            this.Close();
        }
        private void searchButton_Click(object sender, EventArgs e)
        {
            Student requiredStudent = StudentCRUD.GetStudent(registrationNumber.Text);

            if (requiredStudent != null)
            {
                firstName.Text = requiredStudent.firstName;
                lastName.Text = requiredStudent.lastName;
                contact.Text = requiredStudent.contactNumber;
                email.Text = requiredStudent.email;
                status.Text = requiredStudent.status;
            }
            else
            {
                MessageBox.Show($"No data found: { registrationNumber.Text }");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form viewStudentsForm = new Show_Students_Form();
            viewStudentsForm.Show();
            this.Close();
        }

        private void Manage_Students_Button_Click(object sender, EventArgs e)
        {
            Form updateStudentForm = new UpdateStudentForm();
            updateStudentForm.Show();
            this.Close();
        }
    }
}

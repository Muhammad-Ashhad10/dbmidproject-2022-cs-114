﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Student_Data_Management_App.UI.Student_Forms;
using Student_Data_Management_App.UI.CLO_Forms;


namespace Student_Data_Management_App
{
    public partial class Main_Menu : Form
    {
        public Main_Menu()
        {
            InitializeComponent();
        }

        private void Main_Menu_Load(object sender, EventArgs e)
        {

        }

        private void Manage_Students_Button_Click_1(object sender, EventArgs e)
        {
            Form studentForm = new Student_Main_Menu();
            studentForm.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form cloForm = new CLO_Main_Menu();
            cloForm.Show();
            this.Hide();
        }
    }
}
